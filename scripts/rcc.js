/*
* Randomized Communication Complexity Simulator. Purpose was phrased as:
* ** ** ** **
* Design+implement+visualize a simulator for 2-party randomized
* communication protocols (with shared randomness)
* ** ** ** **
*
* */

/* String.prototype.repeat(count) polyfill.
 *   https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/String/repeat */
if (!String.prototype.repeat) {
    String.prototype.repeat = function(count) {
        'use strict';
        if (this == null) {
            throw new TypeError('can\'t convert ' + this + ' to object');
        }
        var str = '' + this;
        count = +count;
        if (count != count) {
            count = 0;
        }
        if (count < 0) {
            throw new RangeError('repeat count must be non-negative');
        }
        if (count == Infinity) {
            throw new RangeError('repeat count must be less than infinity');
        }
        count = Math.floor(count);
        if (str.length == 0 || count == 0) {
            return '';
        }
        // Ensuring count is a 31-bit integer allows us to heavily optimize the
        // main part. But anyway, most current (August 2014) browsers can't handle
        // strings 1 << 28 chars or longer, so:
        if (str.length * count >= 1 << 28) {
            throw new RangeError('repeat count must not overflow maximum string size');
        }
        var rpt = '';
        for (;;) {
            if ((count & 1) == 1) {
                rpt += str;
            }
            count >>>= 1;
            if (count == 0) {
                break;
            }
            str += str;
        }
        return rpt;
    }
}

/* Randomized Communication Complexity */
RCC = {
    state : {
        sharedRandom : '',
        stepCounter : 0,
        alicePosSR : 0,
        bobPosSR : 0,
        bobCommBit : '',
        aliceCommBit : ''
    },

    initSharedSecret : function() {
        var i = 0;
        for (i = 0; i < 1024; i++) { // just make a long random string :)
            var ssin = Math.random();
            RCC.state.sharedRandom = RCC.state.sharedRandom + ssin.toString(2).substring(2);
        }
    },

    resetCommunicationState : function() {
        var state = RCC.state;
        state.stepCounter = 0;
        state.alicePosSR = 0;
        state.bobPosSR = 0;
        state.bobCommBit = '';
        state.aliceCommBit = '';
    },

    initInputChangeListener : function(domId) {
        document.getElementById(domId).addEventListener("keyup", function () {
            var bsv = document.getElementById(domId).value;
            if (bsv == document.getElementById(domId).previousValue)
                return;
            document.getElementById(domId).previousValue = bsv;
            RCC[domId] = RCC.stringToBinStr(bsv);
            if (domId.startsWith("Alice")) {
                RCC.state.aliceSecret = RCC[domId];
            } else if (domId.startsWith("Bob")) {
                RCC.state.bobSecret = RCC[domId];
            }
            RCC.resetCommunicationState();
        });
    },

    communicationStep : function() {
        if (!(RCC.state.aliceSecret && RCC.state.bobSecret) || RCC.state.sharedRandom.length == 0) {
            throw "Alice / Bob secrets and shared randomness must be set.";
        }
        
        if (0 == RCC.state.stepCounter % 2) {
            // Alices turn
            var sp = Math.floor(RCC.state.stepCounter/2) * RCC.state.aliceSecret.length;
            var ep = sp + RCC.state.aliceSecret.length;
            var rndSub = RCC.state.sharedRandom.substring(sp, ep);
            var resBit = RCC.innerProduct(rndSub, RCC.state.aliceSecret);

            RCC.state.stepCounter++;
            RCC.state.alicePosSR = ep;
            RCC.state.aliceCommBit = resBit;
            RCC.state.bobCommBit = '';
        }  else { // Bob's turn
            var sp = Math.floor(RCC.state.stepCounter/2) * RCC.state.bobSecret.length;
            var ep = sp + RCC.state.bobSecret.length;
            var rndSub = RCC.state.sharedRandom.substring(sp, ep);
            var resBit = RCC.innerProduct(rndSub, RCC.state.bobSecret);

            RCC.state.stepCounter++;
            RCC.state.bobPosSR = ep;
            RCC.state.bobCommBit = resBit == RCC.state.aliceCommBit ? '1' : '0';
            RCC.state.aliceCommBit = '';
        }
        return RCC.state;
    }
};

RCC.innerProduct = function(s1, s2) {
    // we assume s1 and s2 to be equal length "bit" strings
    var l1 = s1.length, l2 = s2.length, i = 0, product=[];
    if (l1 != l2)
        throw "Cannot calculate inner product of strings length (" + l1 + ", " + l2 + ")";
    for (i = 0; i < l1; i++) {
        product[i] = RCC.bitProduct(s1.charAt(i), s2.charAt(i));
    }
    var res = product.reduce(function(prevVal, currVal) {
        return RCC.bitSum(prevVal, currVal);
    }, '0');
    return res;
};

RCC.isBit = function(aChar) {
    return ('0' == aChar || '1' == aChar);
};

RCC.bitSum = function(c1, c2) {
    if (RCC.isBit(c1) && RCC.isBit(c2)) {
        return (c1 == c2) ? '0' : '1';
    } else {
        throw "No bit sum for: " + c1 + "," + c2;
    }
};

RCC.bitProduct = function(c1, c2) {
    if (RCC.isBit(c1) && RCC.isBit(c2)) {
        return ('1' == c1 && '1' == c2) ? '1' : '0';
    } else {
        throw "No bit product for: " + c1 + "," + c2;
    }
};

RCC.stringToBinStr = function(s) {
    var r = "";
    for (var i = 0; i < s.length; i++) {
        var bs = s.charCodeAt(i).toString(2);
        if (bs.length < 8) {
            bs = "0".repeat(8-bs.length) + bs;
        }
        r = r + bs;
    }
    return r;
};