var width=800;
var height=500;
var i_width=100;
var i_height=350;


var svg = d3.select("#animation").append("svg")
	.attr("width", width)
	.attr("height",height);


var alice = svg.append('svg:image')
	.attr('class','actor')
	.attr('x', function() {return width-i_width;})
	.attr('y', function() {return height-i_height;})
	.attr('width', i_width)
	.attr('height', i_height)
	.attr("xlink:href", "../images/alice.png");


var bob = svg.append('svg:image')
	.attr('class','actor')
	.attr('x','0')
	.attr('y',function() {return height-i_height;})
	.attr('width', i_width)
	.attr('height', i_height)
	.attr("xlink:href", "../images/bob.png");

var b_gen = svg.append('svg:image')
	.attr('class','generator')
	.attr('x', '0')
	.attr('y', '50')
	.attr('width','100')
	.attr('height','100')
	.attr('xlink:href','../images/generator.png');

var a_gen = svg.append('svg:image')
	.attr('class','generator')
	.attr('x', function() {return width-100;})
	.attr('y', '50')
	.attr('width','100')
	.attr('height','100')
	.attr('xlink:href','../images/generator.png');


svg.append("defs").append("marker")
    .attr("id", "arrow")
    .attr("refX", 4) /*must be smarter way to calculate shift*/
    .attr("refY", 2)
    .attr("markerWidth", 6)
    .attr("markerHeight", 4)
    .attr("orient", "auto")
    .append("path")
        .attr("d", "M 0,0 V 4 L6,2 Z"); //this is actual shape for arrowhead

function initArrows() {
	b_to_a = svg.append("line")
		.attr('class', 'arrow')
		.attr("x1", function () {
			return parseFloat(bob.attr("x")) + i_width;
		})
		.attr("y1", function () {
			return parseFloat(bob.attr("y")) + i_height / 1.8;
		})
		.attr("x2", function () {
			return parseFloat(bob.attr("x")) + i_width;
		})
		.attr("y2", function () {
			return parseFloat(bob.attr("y")) + i_height / 1.8;
		})
		.attr("stroke-width", 3)
		.attr("stroke", "black");

	ba_text = svg.append('text')
		.attr('opacity', 0)
		.attr('x', width / 2)
		.attr('y', function () {
			return parseFloat(bob.attr("y")) + i_height / 1.8;
		})
		.attr("dy", "2em")
		.attr("text-anchor", "middle")
		.text('placeholder');

	a_to_b = svg.append("line")
		.attr('class', 'arrow')
		.attr("x1", alice.attr("x"))
		.attr("y1", function () {
			return parseFloat(bob.attr("y")) + i_height / 2.2;
		})
		.attr("x2", alice.attr("x"))
		.attr("y2", function () {
			return parseFloat(bob.attr("y")) + i_height / 2.2;
		})
		.attr("stroke-width", 3)
		.attr("stroke", "black");

	ab_text = svg.append('text')
		.attr('opacity', 0)
		.attr('x', width / 2)
		.attr('y', function () {
			return parseFloat(bob.attr("y")) + i_height / 2.2;
		})
		.attr("dy", "2em")
		.attr("text-anchor", "middle")
		.text('placeholder');
}


function BAArrow(){
	b_to_a.transition()
		.style("opacity", 1)
		.ease("cubic")
		.duration(1000)
		.attr("x1", function () {
			return parseFloat(bob.attr("x")) + i_width;
		})
		.attr("y1", function () {
			return parseFloat(bob.attr("y")) + i_height / 1.8;
		})
		.attr("x2", alice.attr("x"))
		.attr("y2", function () {
			return parseFloat(bob.attr("y")) + i_height / 1.8;
		})
		.attr("stroke-width", 3)
		.attr("stroke", "black")
		.attr('marker-end', 'url(#arrow)');

}

function ABArrow() {
	a_to_b.transition()
		.style("opacity", 1)
		.ease('cubic')
		.duration(1000)
		.attr("x1", alice.attr("x"))
		.attr("y1", function () {
			return parseFloat(bob.attr("y")) + i_height / 2.2;
		})
		.attr("x2", function () {
			return parseFloat(bob.attr("x")) + i_width;
		})
		.attr("y2", function () {
			return parseFloat(bob.attr("y")) + i_height / 2.2;
		})
		.attr("stroke-width", 3)
		.attr("stroke", "black")
		.attr('marker-end', 'url(#arrow)');
}



var a_rand_text = svg.append('text')
	.attr('class','rtext')
	.attr('opacity',0)
	.attr('x', function() {return width-200;})
	.attr('y', function() {return height-350;})
	.text(' ');


var b_rand_text = svg.append('text')
	.attr('class','rtext')
	.attr('opacity',0)
	.attr('x', function() {return 200;})
	.attr('y', function() {return height-350;})
	.text(' ');

// Multiline text formatting for random string
function formatText(txt,linelength,elem) {
	var i=0;
	d3.selectAll('rtext').selectAll('tspan').remove();
	while(i*linelength < txt.length){
		len=i*linelength;
		var line=txt.slice(len,len+linelength);
		elem.append("tspan")
			.attr("x", elem.attr('x'))
			.attr("y", elem.attr('y'))
			.attr("dy", i * 1.1 + "em").text(line);
		i++;
	}
}

var a_gen_curve1 = svg.append('svg:path')
	.attr('class','link')
	.attr('opacity',0)
	.attr('d',draw_curve(parseFloat(a_gen.attr('x'))
		,parseFloat(a_gen.attr('y'))+50
		,parseFloat(a_rand_text.attr('x'))+30
		,parseFloat(a_rand_text.attr('y')-20),40));

var a_gen_curve1_tl = a_gen_curve1.node().getTotalLength();

var b_gen_curve1 = svg.append('svg:path')
	.attr('class','link')
	.attr('opacity',0)
	.attr('d',draw_curve(parseFloat(b_gen.attr('x'))+100
		,parseFloat(b_gen.attr('y'))+50
		,parseFloat(b_rand_text.attr('x'))+30
		,parseFloat(b_rand_text.attr('y')-20),-40));

var b_gen_curve1_tl = b_gen_curve1.node().getTotalLength();


// Draw curved paths
function draw_curve(Ax, Ay, Bx, By, M) {
	// Find midpoint J
	var Jx = Ax + (Bx - Ax) / 2;
	var Jy = Ay + (By - Ay) / 2;
	var a = Bx - Ax;
	var asign = (a < 0 ? -1 : 1);
	var b = By - Ay;
	//var bsign = (b < 0 ? -1 : 1);
	var theta=1;
	if(b !== a){
		theta = Math.atan(b / a);
	}
	var costheta = asign * Math.cos(theta);
	var sintheta = asign * Math.sin(theta);
	var c = M * sintheta;
	var d = M * costheta;
	var Kx = Jx - c;
	var Ky = Jy + d;
	return "M" + Ax + "," + Ay +
		"Q" + Kx + "," + Ky +
		" " + Bx + "," + By
}

var f_error= false;
var running = false;
var b_prevpos=0;
var a_prevpos=0;
function run() {
	if(running){
		return
	}
	running = true;
	initArrows();

	d3.selectAll('tspan').remove();
	var state = RCC.communicationStep();
	var round=Math.ceil(state.stepCounter/2);
	d3.select('#roundno').html('Round:<br/>'+round);
	console.log(state);
	if (state.stepCounter % 2 == 0) { //Bob-to-Alice interaction
		if(state.bobCommBit == 0){
			d3.select('#certainty').html('Probability of error:<br/>0')
			f_error=true;
		}
		else{
			if(!f_error){
				d3.select('#certainty').html('Probability of error:<br/>'+1/Math.pow(2,round));
			}
		}
		// Get random string
		bobRandom=state.sharedRandom.slice(b_prevpos,state.bobPosSR);
		formatText(bobRandom,10,b_rand_text);
		d3.select('#explainbox')
			.html('Bob gets random string from generator and computes the inner product modulo 2.');

		//Arrow to random
		b_gen_curve1
			.attr("stroke-dasharray", b_gen_curve1_tl + " " + b_gen_curve1_tl)
			.attr("stroke-dashoffset", b_gen_curve1_tl)
			.transition()
			.duration(500)
			.attr('opacity',1)
			.attr("stroke-dashoffset", 0);
		setTimeout(function(){
			b_gen_curve1.attr('marker-end', 'url(#arrow)');
		},400);

		//Random visible
		b_rand_text.transition()
			.duration(400)
			.attr('opacity',1)
			.attr('y',function() {return height-320;});
		//Random invisible
		setTimeout(function(){
			b_rand_text.transition().duration(400).attr('opacity',0).attr('y',function() {return height-350;});
			b_gen_curve1.transition().duration(400).attr('opacity',0).attr('marker-end','');
		},2000);
		b_prevpos=state.bobPosSR;

		// Send bit to Alice
		setTimeout(function() {
			d3.select('#explainbox')
				.html('He compares the bit he got from alice with the one he computed.<br/>' +
					'If they are equal, he replies with 1, else he replies with 0.');
			BAArrow();
			ba_text.transition().duration(400).text(state.bobCommBit).attr('opacity',1);
			setTimeout(function(){
				ba_text.transition().duration(400).attr('opacity',0);
				b_to_a.transition().duration(400).style("opacity", 0);
				running = false;
			},1000)
		}, 2000)

	}
	else{ //Alice-to-Bob interaction

		//Get random string
		var aliceRandom=state.sharedRandom.slice(a_prevpos,state.alicePosSR);
		console.log(aliceRandom);
		formatText(aliceRandom,10,a_rand_text);
		d3.select('#explainbox')
			.html('Alice gets random string from generator and computes the inner product modulo 2.');

		//Arrow to random
		a_gen_curve1
			.attr("stroke-dasharray", a_gen_curve1_tl+ " " + a_gen_curve1_tl)
			.attr("stroke-dashoffset", a_gen_curve1_tl)
			.transition()
			.duration(500)
			.attr('opacity',1)
			.attr("stroke-dashoffset", 0);
		setTimeout(function(){
			a_gen_curve1.attr('marker-end', 'url(#arrow)');
		},400);

		//Random visible
		a_rand_text.transition()
			.duration(400)
			.attr('opacity',1)
			.attr('y',function() {return height-320;});
		//Random invisible
		setTimeout(function(){
			a_rand_text.transition().duration(400).attr('opacity',0).attr('y',function() {return height-350;});
			a_gen_curve1.transition().duration(400).attr('opacity',0).attr('marker-end','');
		},2000);
		a_prevpos=state.alicePosSR;

		//Send bit to Bob
		setTimeout(function() {
			d3.select('#explainbox')
				.html('She sends the computed bit to Bob.');
			ABArrow();
			ab_text.transition().duration(400).text(state.aliceCommBit).attr('opacity',1);
			setTimeout(function(){
				ab_text.transition().duration(400).attr('opacity',0);
				a_to_b.transition().duration(400).style("opacity", 0);
				running = false;
			},1000)
		},2000)


	}
}





